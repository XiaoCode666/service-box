/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOGGER_IMPL_H
#define LOGGER_IMPL_H

#include <map>

#include "klogger/interface/logger.h"
#include "scope_lock.h"
#include "util.h"

using namespace klogger;

class LoggerImpl : public Logger {
public:
  LoggerImpl();
  virtual ~LoggerImpl();

  virtual void destroy();
  virtual Appender *getAppender(const std::string &name);
  virtual Appender *newAppender(const std::string &name,
                                const std::string &attribute);

  /**
   * �Ƴ�������
   * @param name ����������
   */
  void remove(const std::string &name);

public:
  void serviceStart() {}
  void serviceStop() {}

private:
  typedef std::map<std::string, Appender *> AppenderMap;
  AppenderMap _appMap; ///< ������map
  RecursiveLock _lock; ///< ��
};

#endif // LOGGER_IMPL_H
