/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FILE_APPENDER_H
#define FILE_APPENDER_H

#include <cstdarg>
#include <cstdio>
#include <list>
#include <string>

#include "klogger/interface/logger.h"
#include "mode.h"
#include "util.h"
#include <mutex>
#include <thread>
#include <memory>

class LoggerImpl;

using namespace klogger;

class FileAppender : public Appender {
  std::unique_ptr<char[]> buffer_ptr_;

public:
  FileAppender(LoggerImpl *impl, const std::string &name,
               const std::string &attribute);
  virtual ~FileAppender();
  virtual void write(int level, const char *format, ...);
  virtual void setLevel(int level);
  virtual int getLevel();
  virtual void destroy();
  virtual void reload(const std::string &attribute);

private:
  /**
   * 解析配置字符串
   * @param attribute 配置字符串
   */
  void parseAttribute(const std::string &attribute);

  /**
   * 取得配置值
   * @param name 配置键名称
   * @param attribute 配置
   * @return 配置值
   */
  std::string getAttribute(const std::string &name,
                           const std::string &attribute);

  /**
   * 写日志行
   * @param level 日志等级字符串
   * @param format 日志格式
   * @param va_ptr 可变参数宏
   */
  void write(const std::string &level, const char *format, va_list va_ptr);

  /**
   * 打开日志文件
   */
  void open();

  /**
   * 获取最大文件大小
   */
  int getMaxSize(const std::string &str);

private:
  FILE *_fp;              ///< 日志文件描述符
  std::string _file;      ///< 日志文件名称
  std::string _path;      ///< 日志目录名称
  std::string _mode;      ///< 日志模式
  std::string _line;      ///< 日志行前缀
  std::string _name;      ///< 添加器在日志行中显示的名称
  std::string _realName;  ///< 添加器名称
  bool _flush;            ///< flush标志
  bool _async;            ///< 异步标志
  volatile bool _running; ///< 线程运行标志
  LoggerImpl *_impl;      ///< Logger实现类
  Mode _logMode;          ///< 日志模式
  Pattern _pattern;       ///< 日志行前缀匹配器
  std::mutex _fpLock;     ///< 多线程锁
  int _level;             ///< 日志等级
  int _size;              ///< 日志文件长度
  int _maxSize;           ///< 单个日志文件最大长度
  int _index;             ///< 日志文件索引
  bool _useLastFile; ///< 是否使用上一个未达到最大长度的日志文件
  bool _same_log_count{true}; ///< 相同日志追加数量

private:
  struct LogLine {
    char *log{nullptr};
    int prefix_size{0};
  };
  typedef std::list<LogLine> LogList;
  LogList _logList;    ///< 日志行队列
  std::thread _worker; ///< 写入线程
};

#endif // FILE_APPENDER_H
