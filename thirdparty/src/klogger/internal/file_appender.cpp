﻿/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>
#include <limits>
#include <sstream>
#include <unordered_map>
#include <vector>

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
#include <io.h>
#else
#include <fcntl.h>
#include <unistd.h>
#endif // _WIN32

#include "file_appender.h"
#include "logger_impl.h"
#include "util/string_util.hh"

#ifdef max
#undef max
#endif

FileAppender::FileAppender(LoggerImpl *impl, const std::string &name,
                           const std::string &attribute)
    : _fp(0), _file("%y%m%d-%H-%M-%S"), _name("[" + name + "]"),
      _realName(name), _flush(false), _async(true), _running(true), _impl(impl),
      _level(Logger::MIN), _size(0), _maxSize(std::numeric_limits<int>::max()),
      _index(0), _useLastFile(true) {
  // 获取配置
  parseAttribute(attribute);
  if (_async) { // 异步模式
                // 启动异步日志线程
    _worker = std::thread([&]() {
      using LogMap = std::unordered_map<std::string, std::size_t>;
      LogMap log_map_;
      while (_running) {
        bool need_pause = false;
        LogLine line; // 当前日志行
        {
          std::lock_guard guard(_fpLock);
          if (!_logList.empty()) {
            line = _logList.back();
            _logList.pop_back();
          } else {
            need_pause = true;
          }
        }
        if (line.log) { // 有日志行需要写入
          std::string suffix;
          if (_same_log_count) {
            auto it = log_map_.find(line.log + line.prefix_size);
            if (it != log_map_.end()) {
              it->second += 1;
              suffix += "{" + std::to_string(it->second) + "}";
            } else {
              log_map_.emplace(line.log + line.prefix_size, 0);
            }
          }
          fprintf(_fp, "%s%s", line.log, (suffix + LINE_END).c_str());
          delete[] line.log;
          if (_flush) {
            fflush(_fp);
          }
        }
        if (need_pause) {
          sleep(1);
        }
      }
    });
  }
}

FileAppender::~FileAppender() {}

void FileAppender::write(int level, const char *format, ...) {
  if (level < _level) {
    return;
  }
  open(); // 切换或第一次建立文件
  if (!_fp) {
    return;
  }
  va_list va_ptr;
  va_start(va_ptr, format);
  write(getLevelString(level), format, va_ptr);
  va_end(va_ptr);
}

void FileAppender::setLevel(int level) { _level = level; }

int FileAppender::getLevel() { return _level; }

void FileAppender::destroy() {
  if (_async) {
    _running = false; // 停止
    if (_worker.joinable()) {
      _worker.join();
    }
  }
  // 输出所有未被写入的log
  LogList::iterator guard = _logList.begin();
  for (; guard != _logList.end(); guard++) {
    if (_fp) {
      fprintf(_fp, "%s%s", guard->log, LINE_END);
    }
    delete[] guard->log;
  }
  if (_fp) {
    fflush(_fp);
    fclose(_fp);
  }
  _fp = nullptr;
  // 从管理器内删除
  _impl->remove(_realName);
  // 销毁自己
  delete this;
}

static bool pathExists(const std::string &path) {
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
  return !_access(path.c_str(), 0);
#else
  return (access(path.c_str(), F_OK) != -1);
#endif // _WIN32
}

static size_t fileSize(const std::string &path) {
  size_t size = (size_t)-1;
  FILE *fp;
  fp = fopen(path.c_str(), "r");
  if (!fp) {
    return size;
  }
  fseek(fp, 0L, SEEK_END);
  size = ftell(fp);
  fclose(fp);
  return size;
}

void FileAppender::open() {
  std::lock_guard guard(_fpLock);
  if (_logMode.reach() || (_size > _maxSize)) {
    if (_size <= _maxSize) {
      _index = 0;
    }
    // 需要切换文件
    if (_fp) {
      fclose(_fp);
    }
    _fp = 0;
    _size = 0;
  }
  if (!_fp) {
    // 建立日志文件
    std::string fileName;
    std::string rootPath = _path + _logMode.getFileName();
    for (;;) {
      fileName = rootPath;
      std::stringstream ss;
      ss << "." << _index++;
      fileName += ss.str();
      if (!pathExists(fileName)) {
        break;
      }
      if (_useLastFile && (fileSize(fileName) < (size_t)_maxSize)) {
        break;
      }
    }
    _fp = fopen(fileName.c_str(), "a+");
    if (!_fp) {
      return;
    }
  }
}

void FileAppender::write(const std::string &level, const char *format,
                         va_list va_ptr) {
  std::string pattern = _pattern.getPrefix();
  int len = 0;
  if (!buffer_ptr_) {
    buffer_ptr_.reset(new char[1024 * 1024]);
  }
  char* buffer = buffer_ptr_.get();
  if (!level.empty()) { // 日志等级
    memcpy(buffer + len, level.c_str(), (int)level.size());
    len += (int)level.size();
  }
  if (!_name.empty()) { // 日志名称
    memcpy(buffer + len, _name.c_str(), (int)_name.size());
    len += (int)_name.size();
  }
  if (!pattern.empty()) { // 日志行前缀
    memcpy(buffer + len, pattern.c_str(), (int)pattern.size());
    len += (int)pattern.size();
  }
  decltype(len) prefix_size = len;
  int bytes = vsnprintf(buffer + len, sizeof(buffer) - len, format, va_ptr);
  if (0 >= bytes) {
    return;
  }
  len += bytes;
  _size += bytes;
  if (_async) { // 写入到异步队列
    char *log = new char[len + 1];
    memcpy(log, buffer, len);
    log[len] = 0;
    std::lock_guard guard(_fpLock);
    _logList.push_front({log, prefix_size});
  } else { // 同步写入
    std::lock_guard guard(_fpLock);
    fprintf(_fp, "%s%s", buffer, LINE_END);
    if (_flush) {
      fflush(_fp);
    }
  }
}

void FileAppender::reload(const std::string &attribute) {
  parseAttribute(attribute);
}

void FileAppender::parseAttribute(const std::string &attribute) {
  std::vector<std::string> tokens;
  kratos::util::split(attribute, ";", tokens);
  std::vector<std::string>::iterator guard = tokens.begin();
  // 解析日志属性配置
  for (; guard != tokens.end(); guard++) {
    if (std::string::npos != guard->find("file://")) {
      _path = getAttribute("file://", *guard);
      if (_path.at(_path.size() - 1) != SEPERATOR) {
        _path += SEPERATOR;
      }
    } else if (std::string::npos != guard->find("mode=")) {
      _mode = getAttribute("mode=", *guard);
    } else if (std::string::npos != guard->find("file=")) {
      _file = getAttribute("file=", *guard);
    } else if (std::string::npos != guard->find("async=")) {
      std::string async = getAttribute("async=", *guard);
      _async = (async == "true");
    } else if (std::string::npos != guard->find("line=")) {
      _line = getAttribute("line=", *guard);
      _pattern.parse(_line);
    } else if (std::string::npos != guard->find("flush=")) {
      std::string flush = getAttribute("flush=", *guard);
      _flush = (flush == "true");
    } else if (std::string::npos != guard->find("level=")) {
      std::string level = getAttribute("level=", *guard);
      _level = atoi(level.c_str());
    } else if (std::string::npos != guard->find("maxSize=")) {
      std::string maxSize = getAttribute("maxSize=", *guard);
      _maxSize = getMaxSize(maxSize);
    } else if (std::string::npos != guard->find("useLastFile=")) {
      std::string maxSize = getAttribute("useLastFile=", *guard);
      _useLastFile = (maxSize == "true");
    } else if (std::string::npos != guard->find("logCount=")) {
      auto log_count = getAttribute("logCount=", *guard);
      _same_log_count = (log_count == "true");
    }
  }
  if (!_mode.empty()) {
    _logMode.parse(_mode, _file);
  }
}

int FileAppender::getMaxSize(const std::string &str) {
  if (str.empty()) {
    return _maxSize;
  }
  int dem = 1;
  char last = str[str.length() - 1];
  if (last == 'K' || last == 'k') {
    dem = 1024;
  } else if (last == 'M' || last == 'm') {
    dem = 1024 * 1024;
  } else if (last == 'G' || last == 'g') {
    dem = 1024 * 1024 * 1024;
  }
  if (dem > 1) {
    if (str.size() < 2) {
      return _maxSize;
    }
    std::string value;
    value.assign(str, 0, str.size() - 1);
    return (dem * atoi(value.c_str()));
  }
  return atoi(str.c_str());
}

std::string FileAppender::getAttribute(const std::string &name,
                                       const std::string &attribute) {
  size_t pos = attribute.find(name);
  if (pos == std::string::npos) {
    throw LoggerException("invalid attribute");
  }
  return attribute.substr(pos + name.size());
}
