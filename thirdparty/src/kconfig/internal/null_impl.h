#pragma once

#include <string>

#include "attribute_impl.hpp"
#include "config.h"

using namespace kconfig;

/**
 * ��ֵʵ����
 */
class NullImpl : public AttributeImpl<NullAttribute, Attribute::NULL_VAL> {
public:
  NullImpl() {}
  virtual ~NullImpl() {}
  virtual const std::string &name() override {
    static std::string NullString;
    return NullString;
  }
  virtual int getType() override { return 0; }
  virtual AttributePtr doOperate(int op, AttributePtr rhs) override {
    return nullptr;
  }
  virtual auto to_string() -> std::string override { return "null"; }
};
