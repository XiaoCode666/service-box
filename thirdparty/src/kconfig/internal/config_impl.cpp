/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "config_impl.h"

#include "domain.h"
#include "grammar.h"

ConfigImpl::ConfigImpl() {
  _grammar = new Grammar(this);
  _root = 0;
}

ConfigImpl::~ConfigImpl() {
  if (0 != _grammar) {
    delete _grammar;
  }

  _grammar = 0;
  _root = 0;
}

void ConfigImpl::load(const std::string &base, const std::string &file) {
  // if (0 != _grammar) {
  //  delete _grammar;
  //  _grammar = 0;
  //  _root = 0;
  //}

  if (file.empty()) {
    throw ConfigException("invalid file path");
  }

  _base = base;
  _file = file;
  _root = _grammar->parse(base, file);

  if (!_grammar->isOk()) {
    throw ConfigException(_grammar->getLastError());
  }
}

void ConfigImpl::reload(const std::string &base, const std::string &file) {
  if (0 != _grammar) {
    delete _grammar;
    _grammar = 0;
    _root = 0;
  }

  load(base, file);
}

void ConfigImpl::reload() {
  if (0 != _grammar) {
    delete _grammar;
    _grammar = 0;
    _root = 0;
  }

  load(_base, _file);
}

void ConfigImpl::unload() {
  if (0 != _grammar) {
    delete _grammar;
    _grammar = 0;
    _root = 0;
  }
}

bool ConfigImpl::has(const std::string &key) {
  if (0 == _root) {
    return false;
  }

  return _root->has(key);
}

int8_t ConfigImpl::getInt8(const std::string &key) {
  return (int8_t)_root->get(key)->number()->getInt();
}

uint8_t ConfigImpl::getUint8(const std::string &key) {
  return (uint8_t)_root->get(key)->number()->getUint();
}

int16_t ConfigImpl::getInt16(const std::string &key) {
  return (int16_t)_root->get(key)->number()->getInt();
}

uint16_t ConfigImpl::getUint16(const std::string &key) {
  return (uint16_t)_root->get(key)->number()->getUint();
}

int32_t ConfigImpl::getInt32(const std::string &key) {
  return (int32_t)_root->get(key)->number()->getInt();
}

uint32_t ConfigImpl::getUint32(const std::string &key) {
  return (uint32_t)_root->get(key)->number()->getUint();
}

int64_t ConfigImpl::getInt64(const std::string &key) {
  return (int64_t)_root->get(key)->number()->getLlong();
}

uint64_t ConfigImpl::getUint64(const std::string &key) {
  return (uint64_t)_root->get(key)->number()->getULlong();
}

float ConfigImpl::getFloat(const std::string &key) {
  return _root->get(key)->number()->getFloat();
}

double ConfigImpl::getDouble(const std::string &key) {
  return _root->get(key)->number()->getDouble();
}

const std::string &ConfigImpl::getString(const std::string &key) {
  return _root->get(key)->string()->get();
}

bool ConfigImpl::getBool(const std::string &key) {
  return _root->get(key)->boolean()->get();
}

Attribute *ConfigImpl::get(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }
  if (key.empty()) {
    return _root;
  }

  return _root->get(key);
}

NumberAttribute *ConfigImpl::number(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }

  Attribute *attribute = _root->get(key);

  if (0 == attribute) {
    throw ConfigException("key was not found");
  }

  return attribute->number();
}

StringAttribute *ConfigImpl::string(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }

  Attribute *attribute = _root->get(key);

  if (0 == attribute) {
    throw ConfigException("key was not found");
  }

  return attribute->string();
}

ArrayAttribute *ConfigImpl::array(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }

  Attribute *attribute = _root->get(key);

  if (0 == attribute) {
    throw ConfigException("key was not found");
  }

  return attribute->array();
}

TableAttribute *ConfigImpl::table(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }

  Attribute *attribute = _root->get(key);

  if (0 == attribute) {
    throw ConfigException("key was not found");
  }

  return attribute->table();
}

ZoneAttribute *ConfigImpl::zone(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }

  Attribute *attribute = _root->get(key);

  if (0 == attribute) {
    throw ConfigException("key was not found");
  }

  return attribute->zone();
}

BoolAttribute *ConfigImpl::boolean(const std::string &key) {
  if (0 == _root) {
    throw ConfigException("not initialized");
  }

  Attribute *attribute = _root->get(key);

  if (0 == attribute) {
    throw ConfigException("key was not found");
  }

  return attribute->boolean();
}

bool ConfigImpl::register_function(const std::string &name,
                                   ConfigFunction func) {
  _func_map[name] = func;
  return true;
}

ConfigFunction ConfigImpl::get_function(const std::string &name) {
  auto it = _func_map.find(name);
  if (it == _func_map.end()) {
    return nullptr;
  }
  return it->second;
}

Domain *ConfigImpl::get_root_domain() { return _grammar->get_root_domain(); }

void ConfigImpl::serviceStart() {}

void ConfigImpl::serviceStop() {}
