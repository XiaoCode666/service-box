#include "expression.h"
#include "bool_impl.h"
#include "domain.h"
#include "null_impl.h"
#include "number_impl.h"
#include "string_impl.h"

#include <iostream>
#include <stdexcept>

namespace kconfig {

/// BinopPrecedence - This holds the precedence for each binary operator that is
/// defined.
static std::map<char, int> BinopPrecedence = {
    {Token::LOGIC_OR, 38},
    {Token::LOGIC_AND, 39},
    {Token::LOGIC_NOT_EQUAL, 40},
    {Token::LOGIC_EQUAL, 40},
    {Token::LESSER_EQUAL, 50},
    {Token::LARGER_EQUAL, 50},
    {Token::LARGER, 50},
    {Token::LESSER, 50},
    {Token::PLUS, 90},
    {Token::SUB, 90},
    {Token::MUL, 100},
    {Token::DIV, 100},
    {Token::MOD, 100},
    {Token::EXCL_MARK, 110},
};

ExprAST::~ExprAST() {}

NumberExprAST::NumberExprAST(const std::string &val) { str_val_ = val; }

NumberExprAST::~NumberExprAST() {}

AttributePtr NumberExprAST::gen_attribute(Lexer *lexer) {
  return std::make_shared<NumberImpl>(str_val_);
}

VariableExprAST::VariableExprAST(const std::string &name) : name_(name) {}

VariableExprAST::VariableExprAST(const std::string &name,
                                 const std::string &index) {
  name_ = name;
  index_ = index;
}

VariableExprAST::~VariableExprAST() {}

AttributePtr VariableExprAST::gen_attribute(Lexer *lexer) {
  if (name_ == "true" || name_ == "false") {
    return std::make_shared<BoolImpl>(name_ == "true" ? true : false);
  } else {
    if (!index_.empty()) {
      auto attr_ptr = lexer->grammar()->get_root_domain()->get_ptr(name_);
      if (attr_ptr) {
        if (attr_ptr->isArray()) {
          return attr_ptr->array()->get_ptr(std::stoi(index_));
        } else if (attr_ptr->isTable()) {
          return attr_ptr->table()->get_ptr(index_);
        } else {
          throw std::runtime_error("Only table or array support indexer");
        }
      }
    } else {
      return lexer->grammar()->get_root_domain()->get_ptr(name_);
    }
  }
  return nullptr;
}

/// GetTokPrecedence - Get the precedence of the pending binary operator token.
int GetTokPrecedence(int tok);

ExprASTUniPtr ParseIdentifierExpr(Lexer *lexer);

ExprASTUniPtr ParseVariable(const std::string &name, Lexer *lexer);

/// numberexpr ::= number
ExprASTUniPtr ParseNumberExpr(Lexer *lexer);

/// stringexpr ::= '"' literal '"'
ExprASTUniPtr ParseStringExpr(Lexer *lexer);

/// parenexpr ::= '(' expression ')'
ExprASTUniPtr ParseParenExpr(Lexer *lexer);

/// primary
///   ::= identifierexpr
///   ::= numberexpr
///   ::= parenexpr
ExprASTUniPtr ParsePrimary(Lexer *lexer);

/// binoprhs
///   ::= ('+' primary)*
ExprASTUniPtr ParseBinOpRHS(int ExprPrec, ExprASTUniPtr LHS, Lexer *lexer);

BinaryExprAST::BinaryExprAST(int op, ExprASTUniPtr lhs, ExprASTUniPtr rhs)
    : op_(op), lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

BinaryExprAST::~BinaryExprAST() {}

AttributePtr BinaryExprAST::gen_attribute(Lexer *lexer) {
  auto lhs_attr = lhs_->gen_attribute(lexer);
  auto rhs_attr = rhs_->gen_attribute(lexer);
  if (lhs_attr && rhs_attr) {
    if (lhs_attr->getType() != rhs_attr->getType()) {
      throw std::runtime_error("Attribute type mismatch");
    }
    return lhs_attr->doOperate(op_, rhs_attr);
  } else if (!rhs_attr) {
    return lhs_attr;
  } else {
    return lhs_attr;
  }
}

AttributePtr BinaryExprAST::eval(Lexer *lexer, AttributePtr lhs,
                                 AttributePtr rhs) {
  return lhs->doOperate(op_, rhs);
}

CallExprAST::CallExprAST(const std::string &callee,
                         std::vector<ExprASTUniPtr> &args)
    : callee_(callee), args_(std::move(args)) {}

CallExprAST::~CallExprAST() {}

AttributePtr CallExprAST::gen_attribute(Lexer *lexer) {
  // 调用函数返回结果
  return call(lexer);
}

auto CallExprAST::call(Lexer *lexer) -> AttributePtr {
  std::vector<AttributePtr> attr_args;
  for (auto &arg : args_) {
    attr_args.emplace_back(arg->gen_attribute(lexer));
  }
  // 调用函数
  auto func = lexer->grammar()->get_config()->get_function(callee_);
  if (!func) {
    std::string reason("Function not found");
    reason += " " + callee_;
    throw std::runtime_error(reason);
  }
  return func(attr_args);
}

StringExprAST::StringExprAST(const std::string &str) { str_ = str; }

StringExprAST::~StringExprAST() {}

AttributePtr StringExprAST::gen_attribute(Lexer *lexer) {
  return std::make_shared<StringImpl>(str_);
}

/// GetTokPrecedence - Get the precedence of the pending binary operator token.
int GetTokPrecedence(int tok) {
  switch (tok) {
  case Token::PLUS:
  case Token::SUB:
  case Token::MUL:
  case Token::DIV:
  case Token::LARGER:
  case Token::LESSER:
  case Token::LARGER_EQUAL:
  case Token::LESSER_EQUAL:
  case Token::LOGIC_EQUAL:
  case Token::LOGIC_NOT_EQUAL:
  case Token::MOD:
  case Token::LOGIC_AND:
  case Token::LOGIC_OR:
  case Token::EXCL_MARK:
    break;
  default:
    return -1;
  }
  // Make sure it's a declared binop.
  return BinopPrecedence[tok];
}

ExprASTUniPtr ParseIdentifierExpr(Lexer *lexer) {
  auto IdName = lexer->current()->getText();
  lexer->getNext();                                     // eat identifier.
  if (lexer->current()->type() != Token::ARRAY_START) { // Simple variable ref.
    return ParseVariable(IdName, lexer);
  }
  // Call.
  lexer->getNext(); // eat (
  std::vector<ExprASTUniPtr> Args;
  if (lexer->current()->type() != Token::ARRAY_END) {
    while (true) {
      ExprASTUniPtr Arg = ParseExpression(lexer);
      if (!Arg) {
        return nullptr;
      }
      Args.push_back(std::move(Arg));
      if (lexer->current()->type() == Token::ARRAY_END) {
        break;
      }
      if (lexer->current()->type() != Token::COMMA) {
        throw std::runtime_error("Expected ')' or ',' in argument list");
        return nullptr;
      }
      lexer->getNext();
    }
  }
  // Eat the ')'.
  lexer->getNext();
  return ExprASTUniPtr(new CallExprAST(IdName, Args));
}

ExprASTUniPtr ParseVariable(const std::string &name, Lexer *lexer) {
  if (lexer->current()->type() == Token::LSQUARE) {
    // indexer
    std::string str_key;
    lexer->getNext(); // skip '['
    auto expr_ptr = ParseExpression(lexer);
    if (!expr_ptr) {
      throw std::runtime_error("Invalid expression");
    }
    auto attr_ptr = expr_ptr->gen_attribute(lexer);
    if (!attr_ptr) {
      throw std::runtime_error("Invalid expression");
    }
    if (!attr_ptr->isNumber() && !attr_ptr->isString()) {
      throw std::runtime_error("Invalid indexer");
    }
    if (attr_ptr->isString()) {
      str_key = attr_ptr->string()->get();
    }
    if (attr_ptr->isNumber()) {
      str_key = std::to_string(attr_ptr->number()->getLlong());
    }
    if (str_key.empty()) {
      throw std::runtime_error("Need a key for indexer");
    }
    lexer->getNext(); // skip ']'
    return std::make_unique<VariableExprAST>(name, str_key);
  } else {
    return std::make_unique<VariableExprAST>(name);
  }
}

/// numberexpr ::= number
ExprASTUniPtr ParseNumberExpr(Lexer *lexer) {
  ExprASTUniPtr Result =
      std::make_unique<NumberExprAST>(lexer->current()->getText());
  lexer->getNext(); // consume the number
  return Result;
}

ExprASTUniPtr ParseStringExpr(Lexer *lexer) {
  auto token = lexer->getNext();
  if (0 == token) {
    throw std::runtime_error("need a string");
    return nullptr;
  }
  std::string value;
  while (lexer->current() && lexer->current()->type() != Token::QUOTE) {
    value = lexer->current()->getText();
    if (!lexer->getNext()) {
      throw std::runtime_error("need a '\"'");
    }
  }
  lexer->getNext(); // eat '"'
  return std::make_unique<StringExprAST>(value);
}

/// parenexpr ::= '(' expression ')'
ExprASTUniPtr ParseParenExpr(Lexer *lexer) {
  lexer->getNext(); // eat (.
  ExprASTUniPtr V = ParseExpression(lexer);
  if (!V) {
    return nullptr;
  }
  if (lexer->current()->type() != Token::ARRAY_END) {
    throw std::runtime_error("expected ')'");
    return nullptr;
  }
  lexer->getNext(); // eat ).
  return V;
}

/// primary
///   ::= identifierexpr
///   ::= numberexpr
///   ::= parenexpr
ExprASTUniPtr ParsePrimary(Lexer *lexer) {
  switch (lexer->current()->type()) {
  case Token::LITERAL:
    if (std::isdigit(lexer->current()->getText()[0])) {
      return ParseNumberExpr(lexer);
    } else {
      return ParseIdentifierExpr(lexer);
    }
  case Token::ARRAY_START:
    return ParseParenExpr(lexer);
  case Token::QUOTE:
    return ParseStringExpr(lexer);
  case Token::SLASH_STROPHE:
    return std::make_unique<EmptyAST>();
  case Token::EXCL_MARK:
    return ParseBinOpRHS(GetTokPrecedence(Token::EXCL_MARK), nullptr, lexer);
  // TODO 遇到'[', 不支持函数调用后做index操作譬如: method(...)[...]
  default:
    return nullptr;
  }
}

/// binoprhs
///   ::= ('+' primary)*
ExprASTUniPtr ParseBinOpRHS(int ExprPrec, ExprASTUniPtr LHS, Lexer *lexer) {
  // If this is a binop, find its precedence.
  while (1) {
    int TokPrec = GetTokPrecedence(lexer->current()->type());
    // If this is a binop that binds at least as tightly as the current binop,
    // consume it, otherwise we are done.
    if (TokPrec < ExprPrec) {
      return LHS;
    }
    // Okay, we know this is a binop.
    int BinOp = lexer->current()->type();
    lexer->getNext(); // eat binop
                      // Parse the primary expression after the binary operator.
    ExprASTUniPtr RHS = ParsePrimary(lexer);
    if (!RHS) {
      return nullptr;
    }
    // If BinOp binds less tightly with RHS than the operator after RHS, let
    // the pending operator take RHS as its LHS.
    int NextPrec = GetTokPrecedence(lexer->current()->type());
    if (TokPrec < NextPrec) {
      RHS = ParseBinOpRHS(TokPrec + 1, std::move(RHS), lexer);
      if (RHS == nullptr) {
        return nullptr;
      }
    }
    if (BinOp == Token::EXCL_MARK) {
      LHS = std::make_unique<UniExprAST>(BinOp, std::move(RHS));
    } else {
      // Merge LHS/RHS.
      LHS = std::make_unique<BinaryExprAST>(BinOp, std::move(LHS),
                                            std::move(RHS));
    }
  }
}

/// expression
///   ::= primary binoprhs
///
ExprASTUniPtr ParseExpression(Lexer *lexer) {
  auto LHS = ParsePrimary(lexer);
  if (!LHS) {
    return nullptr;
  }
  if (LHS->isEmpty()) {
    return LHS;
  }
  return ParseBinOpRHS(0, std::move(LHS), lexer);
}

AttributePtr GenAttribute(ExprASTUniPtr ast, Lexer *lexer) {
  return ast->gen_attribute(lexer);
}

EmptyAST::EmptyAST() {}

EmptyAST::~EmptyAST() {}

AttributePtr EmptyAST::gen_attribute(Lexer *lexer) {
  return std::make_shared<NullImpl>();
}

UniExprAST::UniExprAST(int op, ExprASTUniPtr rhs)
    : op_(op), rhs_(std::move(rhs)) {}

UniExprAST::~UniExprAST() {}

AttributePtr UniExprAST::gen_attribute(Lexer *lexer) {
  auto rhs_attr = rhs_->gen_attribute(lexer);
  if (rhs_attr) {
    return rhs_attr->doOperate(op_, rhs_attr);
  } else {
    return nullptr;
  }
}

AttributePtr UniExprAST::eval(Lexer *lexer, AttributePtr rhs) {
  return rhs->doOperate(op_, rhs);
}

} // namespace kconfig
