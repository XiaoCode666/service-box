#pragma once

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)

#include <process.h>
#include <string>
#include <tchar.h>
#include <windows.h>

extern VOID WINAPI service_ctrl(DWORD dwCtrlCode);
extern BOOL ReportStatusToSCMgr(DWORD dwCurrentState, DWORD dwWin32ExitCode,
                         DWORD dwWaitHint);
extern void AddToMessageLog(DWORD flags, LPCTSTR lpszMsg);
extern void syslog(WORD flags, const char* msg);
extern LPTSTR GetLastErrorText(LPTSTR lpszBuf, DWORD dwSize);
extern void WINAPI service_main_internal(DWORD dwArgc, LPTSTR *lpszArgv);
extern bool start_as_service();
extern bool install_service_internal();
extern bool check_service();
extern bool uninstall_service_internal();
extern bool start_service();
extern bool stop_service();
extern void set_service_name(const std::string &name);
extern bool is_windows_service();

#endif // _WIN32
