#include "call_tracer_factory_impl.hh"
#include "call_tracer/call_tracer.hh"
#include "config/box_config.hh"
#include "kafka/call_tracer_kafka.hh"
#include "log/call_tracer_log_zipkin.hh"
#include "reporter/call_tracer_zipkin_reporter.hh"

#include <map>

namespace kratos {
namespace service {

CallTracerFactoryImpl::CallTracerFactoryImpl() {}
CallTracerFactoryImpl::~CallTracerFactoryImpl() {}
auto CallTracerFactoryImpl::create_from(CallTracerStoreType store_type,
                                        kratos::config::BoxConfig &config)
    -> CallTracerPtr {
  CallTracerPtr call_tracer_ptr;
  switch (store_type) {
  case CallTracerStoreType::KAFKA:
    call_tracer_ptr = std::make_shared<CallTracerKafka>();
    break;
  case CallTracerStoreType::FILE_ZIPKIN:
    call_tracer_ptr = std::make_shared<CallTracerLogZipkin>();
    break;
  case CallTracerStoreType::ZIPKIN:
    call_tracer_ptr = std::make_shared<CallTracerZipkinReport>();
    break;
  default: // 默认输出类型是文件
    call_tracer_ptr = std::make_shared<CallTracerLogZipkin>();
    break;
  }
  // 启动
  if (!call_tracer_ptr->start(config)) {
    return nullptr;
  }
  return call_tracer_ptr;
}

auto CallTracerFactoryImpl::create_from(kratos::config::BoxConfig &config)
    -> CallTracerPtr {
  static const std::map<std::string, CallTracerStoreType> type_map = {
      {"kafka", CallTracerStoreType::KAFKA},
      {"log_zipkin", CallTracerStoreType::FILE_ZIPKIN},
      {"zipkin", CallTracerStoreType::ZIPKIN},
  };
  std::string type;
  if (config.has_attribute("call_tracer.type")) {
    type = config.get_string("call_tracer.type");
  } else {
    type = "log_zipkin";
  }
  auto it = type_map.find(type);
  if (it == type_map.end()) {
    return nullptr;
  }
  return create_from(it->second, config);
}

auto get_call_tracer_factory() -> CallTracerFactory * {
  static CallTracerFactoryImpl factory;
  return &factory;
}

} // namespace service
} // namespace kratos
