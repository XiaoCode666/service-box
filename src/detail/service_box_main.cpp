﻿#include "argument/box_argument.hh"
#include "box/service_box.hh"
#include "util/frame_delayer.hh"
#include "util/time_util.hh"
#include "util/os_util.hh"

int service_box_main(int argc, const char **argv) {
  kratos::service::ServiceBox box;
  // 启动容器
  if (!box.start(argc, argv)) {
    box.stop();
    return -1;
  }
  // 获取帧率配置
  const auto &argument = box.get_argument();
  auto frame = argument.get_max_frame();
  // 启动延时器
  kratos::util::FrameDelayer delayer(frame);
  while (true) {
    if (box.is_wait_stop()) {
      // 收到关闭信号
      break;
    }
    if (frame != argument.get_max_frame()) {
        // 检查帧率设置是否变化,变化则重置
        frame = argument.get_max_frame();
        delayer.reset_frame(frame);
    }
    auto now_ms = delayer.begin_frame();
    kratos::util::set_frame_begin(now_ms);
    box.update(now_ms);
    kratos::util::set_frame_end(delayer.end_frame());
  }
  box.stop();
  return 0;
}
