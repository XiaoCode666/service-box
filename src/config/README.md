1. [接口文件](box_config.hh)
2. [配置说明](../../README-configuration.md)

## 用户自定义配置

可以通过BoxConfig接口获取用户自定义配置，假设有如下的用户配置:

```

MyConfig = {
    str = "string value"
    num = 2345
    array = ("a", "b")
    table = <"a":1, "b":2>
}

```

获取BoxConfig接口:

```
auto& config_ref = getContext()->get_config();
```

获取用户配置:

```
auto str = config_ref.get_string("MyConfig.str");
auto num = config_ref.get_number<int>("MyConfig.num");
auto array = config_ref.get_array<std::string>("MyConfig.array");
auto table = config_ref.get_table<int>("MyConfig.array");
```

