#pragma once

#include "jsonhpp/json.hpp"
#include "tuple_util.hpp"

#include <algorithm>
#include <array>
#include <cstdint>
#include <deque>
#include <forward_list>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace kratos {
namespace util {

template <typename... ArgsT>
inline static auto format(const std::string &fmt, ArgsT &&...args)
    -> std::string;

#define BasicOp(type)                                                          \
  template <typename ItemType> inline void operator()(const type &item);

#define BasicImpl(type)                                                        \
  template <> inline void Pusher::operator()(const type &item) {               \
    str_vec_ptr_->emplace_back(                                                \
        std::make_unique<std::string>(std::to_string(item)));                  \
  }

#define NoPairContainer(type)                                                  \
  template <typename ElemT> inline void operator()(const std::type<ElemT> &vec);

#define PairContainer(type)                                                    \
  template <typename KeyT, typename ValueT>                                    \
  inline void operator()(const std::type<KeyT, ValueT> &map);

namespace format_internal {
class Pusher {
  std::vector<std::unique_ptr<std::string>> *str_vec_ptr_{
      nullptr}; ///< std::vector<std::unique_ptr<std::string>>指针

public:
  /**
   * 构造.
   *
   * \param ss std::stringstream指针
   */
  Pusher(std::vector<std::unique_ptr<std::string>> *str_vec_ptr) {
    str_vec_ptr_ = str_vec_ptr;
  }
  /**
   * 仿函数.
   *
   * \param item 调用参数
   */
  template <typename ItemType> inline void operator()(const ItemType &item) {
    std::stringstream ss;
    // Convert to string
    ss << item;
    str_vec_ptr_->emplace_back(std::make_unique<std::string>(ss.str()));
  }

  template <typename... ArgsT>
  inline void operator()(const std::tuple<ArgsT...> args) {
    std::vector<std::unique_ptr<std::string>> str_vec;
    str_vec.reserve(std::tuple_size<decltype(args)>::value);
    tuple_foreach(args, format_internal::Pusher(&str_vec));
    std::size_t i = 1;
    std::string content;
    content.append("[");
    for (const auto &v : str_vec) {
      if (i < str_vec.size()) {
        content.append(*v).append(", ");
      } else {
        content.append(*v);
      }
      i += 1;
    }
    content.append("]");
    str_vec_ptr_->emplace_back(std::make_unique<std::string>(content));
  }

  template <typename ElemT, std::size_t COUNT>
  inline void operator()(const std::array<ElemT, COUNT> &arr) {
    std::size_t i = 1;
    std::string content;
    content.append("[");
    for (const auto &v : arr) {
      if (i < arr.size()) {
        content.append(format("{}", v)).append(", ");
      } else {
        content.append(format("{}", v));
      }
      i += 1;
    }
    content.append("]");
    str_vec_ptr_->emplace_back(std::make_unique<std::string>(content));
  }

  BasicOp(std::string);
  BasicOp(char *const);
  BasicOp(std::int8_t);
  BasicOp(std::uint8_t);
  BasicOp(std::int16_t);
  BasicOp(std::uint16_t);
  BasicOp(std::int32_t);
  BasicOp(std::uint32_t);
  BasicOp(std::int64_t);
  BasicOp(std::uint64_t);
  BasicOp(bool);
  BasicOp(float);
  BasicOp(double);

  NoPairContainer(vector);
  NoPairContainer(list);
  NoPairContainer(set);
  NoPairContainer(queue);
  NoPairContainer(deque);
  NoPairContainer(unordered_set);
  NoPairContainer(initializer_list);
  NoPairContainer(forward_list);
  NoPairContainer(multiset);
  NoPairContainer(unordered_multiset);

  PairContainer(map);
  PairContainer(unordered_map);
  PairContainer(multimap);
  PairContainer(unordered_multimap);
};
} // namespace format_internal

template <typename T, std::size_t COUNT>
inline static auto to_array(const T (&carray)[COUNT]) -> std::array<T, COUNT> {
  std::array<T, COUNT> stdarray;
  std::copy(std::begin(carray), std::end(carray), std::begin(stdarray));
  return stdarray;
}

#define sarray(carray) kratos::util::to_array(carray)

/**
 *template class to test whether class has uuid method
 */
template <typename T> struct has_to_json_method {
private:
  typedef std::true_type yes;
  typedef std::false_type no;

  template <typename U>
  static auto test(int)
      -> decltype(std::declval<U>().has_to_json_method() == 1, yes());
  template <typename> static no test(...);

public:
  static constexpr bool value = std::is_same<decltype(test<T>(0)), yes>::value;
};

/*
 * format string like c++20 std::format
 */
template <typename... ArgsT>
inline static auto format(const std::string &fmt, ArgsT &&...args)
    -> std::string {
  static const std::size_t TOKEN_N = 2;
  static const char *TOKEN{"{}"};
  static const std::size_t RESERVED_N = 8;

  std::size_t arg_count = 0, last_pos = 0, pos = 0;
  std::size_t str_size = 0;
  std::vector<std::string_view> view_vec;
  view_vec.reserve(RESERVED_N);

  //
  // Find all '{}', store charater in a vector
  //
  while ((pos = fmt.find(TOKEN, last_pos)) != std::string::npos) {
    arg_count += 1;
    view_vec.emplace_back(
        std::string_view(fmt.c_str() + last_pos, pos - last_pos));
    str_size += pos - last_pos;
    last_pos = pos + TOKEN_N;
  }
  if (last_pos < fmt.size()) {
    view_vec.emplace_back(
        std::string_view(fmt.c_str() + last_pos, fmt.size() - last_pos));
    str_size += fmt.size() - last_pos;
  }
  auto tuple_args = std::make_tuple(args...);
  auto tuple_size = std::tuple_size<decltype(tuple_args)>::value;
  std::vector<std::unique_ptr<std::string>> str_vec;
  str_vec.reserve(tuple_size);
  //
  // Check the count of argument
  //
#ifdef UNSAFE_CODE
  if (arg_count != tuple_size) {
#if defined(DEBUG) || defined(_DEBUG)
    throw std::runtime_error("format method argument mismatch");
#else
    return "";
#endif // defined(DEBUG) || defined(_DEBUG)
  }
#endif // UNSAFE_CODE

  //
  // Merge result, this is a low efficiency and ugly version but it works...
  //
  std::string result;
  decltype(arg_count) iter_n = arg_count > tuple_size ? tuple_size : arg_count;
  if (iter_n > 0) {
    tuple_foreach(tuple_args, format_internal::Pusher(&str_vec));
    for (const auto &item : str_vec) {
      str_size += item->size();
    }
    result.reserve(str_size);
    for (decltype(iter_n) i = 0; i < iter_n; i++) {
      result.append(view_vec[i].begin(), view_vec[i].end());
      result.append(str_vec[i]->begin(), str_vec[i]->end());
    }
    for (auto i = iter_n; i < view_vec.size(); i++) {
      result.append(view_vec[i].begin(), view_vec[i].end());
    }
    return result;
  } else {
    return fmt;
  }
}

// The macro to format string
#define sformat(...) kratos::util::format(__VA_ARGS__)

/*
 * format string and print to stdout
 */
template <typename... ArgsT>
inline static auto format_print(const std::string &fmt, ArgsT &&...args)
    -> void {
  std::cout << format(fmt, std::forward<ArgsT>(args)...) << std::endl;
}

// The macro to print string
#define sprint(...) kratos::util::format_print(__VA_ARGS__);

/*
 * get formatted string size in bytes
 */
template <typename... ArgsT>
inline static auto format_size(const std::string &fmt, ArgsT &&...args)
    -> std::size_t {
  static const std::size_t TOKEN_N = 2;
  static const char *TOKEN{"{}"};

  std::size_t arg_count = 0, last_pos = 0, pos = 0;

  //
  // Find all '{}', store charater in a vector
  //
  while ((pos = fmt.find(TOKEN, last_pos)) != std::string::npos) {
    arg_count += 1;
    last_pos = pos + TOKEN_N;
  }
  auto tuple_args = std::make_tuple(args...);
  auto tuple_size = std::tuple_size<decltype(tuple_args)>::value;
  std::vector<std::unique_ptr<std::string>> str_vec;
  str_vec.reserve(tuple_size);
  //
  // Check the count of argument
  //
#ifdef UNSAFE_CODE
  if (arg_count != tuple_size) {
#if defined(DEBUG) || defined(_DEBUG)
    throw std::runtime_error("format method argument mismatch");
#else
    return "";
#endif // defined(DEBUG) || defined(_DEBUG)
  }
#endif // UNSAFE_CODE

  decltype(arg_count) iter_n = arg_count > tuple_size ? tuple_size : arg_count;
  if (iter_n > 0) {
    tuple_foreach(tuple_args, format_internal::Pusher(&str_vec));
    std::size_t str_size{0};
    for (const auto &item : str_vec) {
      str_size += item->size();
    }
    return str_size;
  } else {
    return fmt.size();
  }
}

namespace format_internal {

#define NoPairImpl(type)                                                       \
  template <typename ElemT>                                                    \
  inline void Pusher::operator()(const std::type<ElemT> &vec) {                \
    nlohmann::json json(vec);                                                  \
    str_vec_ptr_->emplace_back(std::make_unique<std::string>(json.dump()));    \
  }

#define PairImpl(type)                                                         \
  template <typename KeyT, typename ValueT>                                    \
  inline void Pusher::operator()(const std::type<KeyT, ValueT> &map) {         \
    nlohmann::json json(map);                                                  \
    str_vec_ptr_->emplace_back(std::make_unique<std::string>(json.dump()));    \
  }

/**
 * 仿函数.
 *
 * \param item 调用参数
 */
template <> inline void Pusher::operator()(const std::string &item) {
  // Convert to string
  str_vec_ptr_->emplace_back(std::make_unique<std::string>(item));
}
/**
 * 仿函数.
 *
 * \param item 调用参数
 */
template <> inline void Pusher::operator()(const char *const &item) {
  // Convert to string
  str_vec_ptr_->emplace_back(std::make_unique<std::string>(item));
}

/**
 * 仿函数.
 *
 * \param item 调用参数
 */
template <> inline void Pusher::operator()(const bool &item) {
  // Convert to string
  if (item) {
    str_vec_ptr_->emplace_back(std::make_unique<std::string>("true"));
  } else {
    str_vec_ptr_->emplace_back(std::make_unique<std::string>("false"));
  }
}

BasicImpl(std::int8_t);
BasicImpl(std::uint8_t);
BasicImpl(std::int16_t);
BasicImpl(std::uint16_t);
BasicImpl(std::int32_t);
BasicImpl(std::uint32_t);
BasicImpl(std::int64_t);
BasicImpl(std::uint64_t);
BasicImpl(float);
BasicImpl(double);

NoPairImpl(vector);
NoPairImpl(list);
NoPairImpl(set);
NoPairImpl(queue);
NoPairImpl(deque);
NoPairImpl(unordered_set);
NoPairImpl(initializer_list);
NoPairImpl(forward_list);
NoPairImpl(multiset);
NoPairImpl(unordered_multiset);

PairImpl(map);
PairImpl(unordered_map);
PairImpl(multimap);
PairImpl(unordered_multimap);

} // namespace format_internal

template <typename T> inline T _lexical_cast(const std::string &s) {}

#define _LEX_CAST(type, fn)                                                    \
  template <> inline type _lexical_cast<type>(const std::string &s) {          \
    try {                                                                      \
      return (type)std::fn(s);                                                 \
    } catch (...) {                                                            \
      return (type)(0);                                                        \
    }                                                                          \
  }

_LEX_CAST(std::int8_t, stol);
_LEX_CAST(std::int16_t, stol);
_LEX_CAST(std::int32_t, stol);
_LEX_CAST(std::int64_t, stoll);
_LEX_CAST(std::uint8_t, stoul);
_LEX_CAST(std::uint16_t, stoul);
_LEX_CAST(std::uint32_t, stoul);
_LEX_CAST(std::uint64_t, stoull);
_LEX_CAST(float, stof);
_LEX_CAST(double, stod);

#ifdef _MSC_VER
_LEX_CAST(long, stol);
_LEX_CAST(unsigned long, stoul);
#endif // _MSC_VER

#define slexical_cast kratos::util::_lexical_cast
#define sstoi(s) kratos::util::_lexical_cast<std::int32_t>(s)
#define sstol(s) kratos::util::_lexical_cast<std::int32_t>(s)
#define sstoul(s) kratos::util::_lexical_cast<std::uint32_t>(s)
#define sstoull(s) kratos::util::_lexical_cast<std::uint64_t>(s)
#define sstof(s) kratos::util::_lexical_cast<float>(s)
#define sstod(s) kratos::util::_lexical_cast<double>(s)

} // namespace util
} // namespace kratos

template <typename... ArgsT>
inline static decltype(auto) sformat_size(const std::string &fmt,
                                          ArgsT &&...args) {
  return kratos::util::format_size(fmt, std::forward<ArgsT>(args)...);
}

template <typename T>
inline static decltype(auto) jarray_ton(const nlohmann::json &json_value,
                                        std::size_t index) {
  if (!json_value.is_array()) {
    return T(0);
  }
  if (index >= json_value.size()) {
    return T(0);
  }
  if (nullptr == json_value[index]) {
    return T(0);
  }
  if (!json_value[index].is_string()) {
    return T(0);
  }
  std::string s = json_value[index];
  return kratos::util::_lexical_cast<T>(s);
}

#define jarraytoi(v, i) jarray_ton<std::int32_t>(v, i)
#define jarraytol(v, i) jarray_ton<std::int32_t>(v, i)
#define jarraytoul(v, i) jarray_ton<std::uint32_t>(v, i)
#define jarraytoull(v, i) jarray_ton<std::uint64_t>(v, i)
#define jarraytof(v, i) jarray_ton<float>(v, i)
#define jarraytod(v, i) jarray_ton<double>(v, i)
