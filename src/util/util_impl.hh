﻿#pragma once

#include "util.hh"

namespace kratos {
namespace service {

/**
 * @brief 工具接口实现
 */
class UtilImpl : public Util {
public:
  UtilImpl();
  virtual ~UtilImpl();
  virtual void splitOf(const std::string &src, const std::string &delim,
                       std::vector<std::string> &result) override;
  virtual void split(const std::string &src, const std::string &delim,
                     std::vector<std::string> &result) override;
  virtual void splitBy(const std::string &src, char delim,
                       std::vector<std::string> &result) override;
  virtual bool endWith(const std::string &src, const std::string &s) override;
  virtual bool startWith(const std::string &src, const std::string &s) override;
  virtual std::string remove(const std::string &src,
                             const std::string &s) override;
  virtual std::string trim(const std::string &src,
                           const std::string &delim = " ") override;
  virtual std::string ltrim(const std::string &src,
                            const std::string &delim = " ") override;
  virtual std::string rtrim(const std::string &src,
                            const std::string &delim = " ") override;
  virtual std::string replace(const std::string &src,
                              const std::string &pattern,
                              const std::string &dest) override;
  virtual std::string get_file_name(const std::string &src) override;
  virtual std::string get_file_full_name(const std::string &src) override;
  virtual std::string get_path(const std::string &src) override;
  virtual bool has_sub_string(const std::string &src,
                              const std::string &sub) override;
  virtual bool isnumber(const std::string &s) override;
  virtual bool is_ip_address(const std::string &s) override;
  virtual bool is_domain_address(const std::string &s) override;
  virtual bool is_valid_host_config(const std::string &s) override;
  virtual bool get_host_config(const std::string &s, std::string &ip,
                               int &port) override;
  virtual std::string demangle(const std::string &name) override;
  virtual bool is_date_string_fmt1(const std::string &date_string) override;
  virtual bool is_date_string_fmt2(const std::string &date_string) override;
  virtual std::string readable_size(std::size_t size) override;
  virtual std::size_t
  from_readable_size_string(const std::string &size_string) override;
  virtual bool get_json_root(const std::string &json_string, Json::Value &root,
                             std::string &error) override;
  virtual bool get_json_root_from_file(const std::string &json_file_path,
                                       Json::Value &root,
                                       std::string &error) override;
  virtual std::string get_host_ip(const std::string &host) override;
  virtual std::string get_binary_path() override;
  virtual std::string get_binary_name() override;
  virtual bool
  get_file_in_directory(const std::string &directory, const std::string suffix,
                        std::vector<std::string> &fileNames) override;
  virtual bool is_path_exists(const std::string &path) override;
  virtual bool remove_file(const std::string &filePath) override;
  virtual std::string complete_path(const std::string &base,
                                    const std::string &sub) override;
  virtual std::string complete_path_url(const std::string &base,
                                        const std::string &sub) override;
  virtual bool rename_file(const std::string &srcFileName,
                           const std::string &newFileName) override;
  virtual std::string get_file_ext(const std::string &fileName) override;
  virtual std::uint32_t get_random_uint32(std::uint32_t a,
                                          std::uint32_t b) override;
  virtual auto make_dir(const std::string &path) -> bool override;
  virtual auto rm_empty_dir(const std::string &path) -> bool override;
  virtual auto get_pid() -> int override;
  virtual auto get_current_stack_trace_info(int depth = 64)
      -> std::string override;
  virtual auto get_last_modify_time(const std::string &path)
      -> std::time_t override;
  virtual auto get_file_content(const std::string &file_path, int line,
                                int related_line, std::string &content)
      -> bool override;
  virtual auto try_lock_file(const std::string &file_path) -> bool override;
  virtual auto try_unlock_file(const std::string &file_path, bool is_remove)
      -> bool override;
  virtual auto unlock_all_file(bool is_remove) -> void override;
  virtual auto create_uuid() -> std::string override;
};

} // namespace service
} // namespace kratos
