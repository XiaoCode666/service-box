# Protobuf lua module

提供Lua表与protobuf::Message之间的转换.

## 导入

```
local pbc = require("pbc")
```

## init

初始化.

```
local ret = pbc.init("The directory for all .proto files")
```

## decode

```
local tbl = pbc.decode("package.MessageName", bytes)
```

bytes为protobuf::Message字节流, 返回值为Lua表

## encode

```
local bytes = pbc.encode("package.MessageName", tbl)
```

tbl为Lua表, bytes为字节流

