#pragma once

#include "bucket_token_defines.hh"

namespace kratos {
namespace util {

class TokenProducer;

/**
 * 令牌消费者.
 */
class TokenConsumer {
public:
  virtual ~TokenConsumer() {}
  /**
   * 取出并销毁一个令牌.
   * 
   * \retval true 成功
   * \retval false 令牌耗尽
   */
  virtual auto consume() -> bool = 0;
  /**
   * 获取令牌的产生器s.
   * 
   * \return 令牌产生器
   */
  virtual auto get_producer() -> TokenProducerPtr = 0;
};

} // namespace util
} // namespace kratos
