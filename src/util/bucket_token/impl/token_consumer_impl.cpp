#include "token_consumer_impl.hh"
#include "util/bucket_token/token_producer.hh"

namespace kratos {
namespace util {
TokenConsumerImpl::TokenConsumerImpl(TokenProducerPtr token_producer_ptr) {
  token_producer_ = token_producer_ptr;
}
TokenConsumerImpl::~TokenConsumerImpl() {}
auto TokenConsumerImpl::consume() -> bool {
  if (token_producer_) {
    return token_producer_->consume();
  }
  return false;
}
auto TokenConsumerImpl::get_producer() -> TokenProducerPtr {
  return token_producer_;
}
} // namespace util
} // namespace kratos
