# Console控制台

外部Console客户端可以连接Console控制台获取ServiceBox及运行在之上服务的信息、开关、选项，并可以操作开关、选项在调试期调整参数。

# 默认的Console客户端页面

## 信息

![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/102503_32a5728d_467198.png "屏幕截图.png")

## 内存
![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/102558_1f805120_467198.png "屏幕截图.png")

## 服务
![](https://images.gitee.com/uploads/images/2020/1112/102623_40890db0_467198.png "屏幕截图.png")

## 调用
![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/102652_6b2606ed_467198.png "屏幕截图.png")

## 开关、选项

![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/102717_41f6e735_467198.png "屏幕截图.png")

开启脚本调试器后可以对脚本服务内的脚本进行调试：
![](https://images.gitee.com/uploads/images/2020/1211/192001_32bd6085_467198.png "屏幕截图.png")

## 更新

![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/102909_4efb26ad_467198.png "屏幕截图.png")

# 添加开关、选项

```
console = getContext()->new_box_console("ServiceDynamicImpl");
// 添加一个自定义开关
console->add_switch("test",
{
  [&](kratos::console::Console& console, kratos::console::ConsoleSwitch& cs)->void {
     cs.set_display_name("sample");
     cs.set_on_off(true);
     cs.set_tips("i'm a tips");
  },
  [&](kratos::console::Console& console, bool on_off, std::string& result)->bool {
     return true;
  }
});

// 添加一个自定义选项
console->add_selection("test_selection",
{
  [&](kratos::console::Console& console, kratos::console::ConsoleSelection& cs)->void {
     cs.set_display_name("sample_selection");
     cs.add_selection({"a", "a", "b", "c"});
     cs.set_tips("i'm a selection");
  },
  [&](kratos::console::Console& console, const std::string& name, std::string& result)->bool {
     return true;
  }
});
```


