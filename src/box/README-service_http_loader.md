# kratos::service::ServiceHttpLoader

service box服务更新

## 设计要点

1. 有一个独立的工作线程用于检测版本、下载
2. 服务的更新在主线程内进行
3. 更新必须满足服务接口不变
4. 新版本服务加载或启动失败将被自动回滚到老版本

## 版本管理

1. 工作线程每隔```remote_repo_check_interval_```毫秒访问```version_api_```拉取版本文件
2. 版本文件为JSON格式:

```
{
    "version":版本号，字符串
    "bundles":
    [
        "uuid":服务UUID，字符串
        "name":dll/so文件名，字符串
    ]
}
```
3. 检测到新版本后工作线程访问```remote_service_repo_dir_```下载bundle, 下载下来的bundle将被放在```temp_service_dir_/temp_bundle_dir```目录下的
version[版本号]目录下，每个bundle将被分步下载，WebService API为: ```http://remote_service_repo_dir_/{bundle name}?version={版本号}```
4. 下载完成后，工作线程通知主线程卸载就版本bundle，然后加载新版本bundle

## 启动强制更新

如果配置文件内配置属性```service.is_open_remote_update="true"```, 则每次服务容器启动时会进行一次版本检查更新, 最新的版本文件配置为```service.remote_service_repo_latest_version_api```

## 配置文件字段

在配置文件的service域内:
```
service = {
    ...
    remote_service_repo_version_api = "http://localhost/version.json"
    remote_service_repo_dir = "http://localhost/repo"
    remote_service_repo_latest_version_api = "http://localhost/latest_version.json"
    is_open_remote_update = "false"
    remote_repo_check_interval = 60
    ...
}
```

