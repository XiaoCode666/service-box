#pragma once

#include <string>

#define VERSION_MAJOR 0 // major
#define VERSION_MINOR 3 // minor
#define VERSION_PATCH 0 // patch

/**
 * Returns version number string
 * 
 * \return version number string
 */
static inline std::string get_version_string() {
  return "version " +
      std::to_string(VERSION_MAJOR) +
      "." +
      std::to_string(VERSION_MINOR) +
      "." +
      std::to_string(VERSION_PATCH) + "-alpha";
}
