#pragma once

#include <cstdint>

namespace kratos {
namespace service {

struct ListenRequest {
  char *name = nullptr;
  char *host = nullptr;
  int port;

  void clear() const;
};

struct ListenResponse {
  char *name = nullptr;
  std::uint64_t channel_id;
  bool success;
  void clear() const;
};

struct AcceptNotify {
  char* name = nullptr;
  std::uint64_t channel_id;
  char *local_ip = nullptr;
  std::int32_t local_port;
  char *peer_ip = nullptr;
  std::int32_t peer_port;
  void clear() const;
};

struct ConnectRequest {
  char *name = nullptr;
  char *host = nullptr;
  int port;
  int timeout;
  void clear() const;
};

struct ConnectResponse {
  char *name = nullptr;
  std::uint64_t channel_id;
  bool success;
  char *local_ip = nullptr;
  std::int32_t local_port;
  char *peer_ip = nullptr;
  std::int32_t peer_port;
  void clear() const;
};

struct RecvDataNotify {
  std::uint64_t channel_id;
  char *data_ptr = nullptr;
  int length;
  void clear() const;
};

struct SendDataNotify {
  std::uint64_t channel_id;
  char *data_ptr = nullptr;
  int length;
  void clear() const;
};

struct CloseNotify {
  std::uint64_t channel_id;
  void clear() const {}
};

struct CloseRequest {
  std::uint64_t channel_id;
  void clear() const {}
};

enum class NetEvent {
  listen_request = 1,
  listen_response,
  accept_notify,
  connect_request,
  connect_response,
  recv_data_notify,
  send_data_notify,
  close_notify,
  close_request,
};

struct NetEventData {
  NetEvent event_id;
  union {
    ListenRequest listen_request;
    ListenResponse listen_response;
    AcceptNotify accept_notify;
    ConnectRequest connect_request;
    ConnectResponse connect_response;
    RecvDataNotify recv_data_notify;
    SendDataNotify send_data_notify;
    CloseNotify close_notify;
    CloseRequest close_request;
  };

  void clear() const;
};

} // namespace service
} // namespace kratos
