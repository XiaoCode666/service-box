#include "udp_channel_loop.hh"
#include "udp_channel.hh"
#include "udp_os.hh"
#include "udp_socket.hh"
#include "util/object_pool.hh"
#include <cstdlib>
#include <iostream>

namespace kratos {
namespace network {

UdpChannelLoop::UdpChannelLoop() { udp_os_initialize(); }

UdpChannelLoop::~UdpChannelLoop() {
  for (auto it : channelMap_) {
    auto channel = it.second;
    if (channel->getType() == UdpType::Acceptor) {
      box_dispose(dynamic_cast<UdpAcceptorChannel *>(channel));
    } else if (channel->getType() == UdpType::Connector) {
      box_dispose(dynamic_cast<UdpConnectorChannel *>(channel));
    } else {
      box_dispose(dynamic_cast<UdpRemoteChannel *>(channel));
    }
  }
  udp_os_deinitialize();
}

void UdpChannelLoop::update(AcceptFunction accepted, ReceiveFunction received) {
  for (auto it : channelMap_) {
    auto channel = it.second;
    channel->update();
    if (channel->getType() == UdpType::Acceptor) {
      _acceptorReceive(channel, accepted, received);
    } else if (channel->getType() == UdpType::Connector) {
      _connectorReceive(channel, received);
    }
  }
}

std::uint64_t UdpChannelLoop::newAcceptor(const std::string &ip,
                                          std::uint16_t port,
                                          const std::string &name,
                                          std::size_t /*netBufSize*/) {
  auto channel = allocate<UdpAcceptorChannel>();
  if (!channel->accept(ip, port)) {
    box_dispose(channel);
    return 0;
  }
  auto &address = channel->getLocalAddress();
  channelMap_[address.getID()] = channel;
  listener_name_map_[address.getID()] = name;
  return address.getID();
}

std::uint64_t UdpChannelLoop::newConnector(const std::string &ip,
                                           std::uint16_t port,
                                           std::size_t /*netBufSize*/) {
  auto channel = allocate<UdpConnectorChannel>();
  if (!channel->connect(ip, port)) {
    box_dispose(channel);
    return 0;
  }
  channelMap_[channel->getID()] = channel;
  return channel->getID();
}

void UdpChannelLoop::send(std::uint64_t id, const char *data,
                          std::size_t length) {
  auto it = channelMap_.find(id);
  if (it == channelMap_.end()) {
    return;
  }
  it->second->send(data, length);
}

void UdpChannelLoop::close(std::uint64_t id) {
  auto it = channelMap_.find(id);
  if (it == channelMap_.end()) {
    return;
  }
  auto channel = it->second;
  if (channel->getType() == UdpType::Acceptor) {
    return;
  }
  box_dispose(channel);
  channelMap_.erase(it);
}

bool UdpChannelLoop::exists(std::uint64_t id) {
  return (channelMap_.find(id) != channelMap_.end());
}

void UdpChannelLoop::_acceptorReceive(UdpChannel *channel,
                                      AcceptFunction accepted,
                                      ReceiveFunction received) {
  std::size_t length = sizeof(buffer_);
  SocketAddress socketAddress;
  auto &socket = channel->getSocket();
  while (socket.recvfrom(buffer_.get(), length, socketAddress) && (length > 0)) {
    UdpAddress address(socketAddress);
    auto exist = channelMap_.find(address.getID());
    if (exist == channelMap_.end()) {
      channelMap_[address.getID()] =
          kratos::allocate<network::UdpRemoteChannel>(address, &socket);
      // 新接收的连接
      accepted(address.getID(),
               listener_name_map_[channel->getLocalAddress().getID()]);
      // 接收事件
      received(address.getID(), buffer_.get(), (std::uint32_t)length);
    } else {
      // 接收事件
      received(address.getID(), buffer_.get(), (std::uint32_t)length);
    }
  }
}

void UdpChannelLoop::_connectorReceive(UdpChannel *channel,
                                       ReceiveFunction received) {
  std::size_t length = sizeof(buffer_);
  SocketAddress socketAddress;
  auto &socket = channel->getSocket();
  while (socket.recvfrom(buffer_.get(), length, socketAddress) && (length > 0)) {
    // connector接受到数据
    auto connector = dynamic_cast<UdpConnectorChannel *>(channel);
    // 接收事件
    received(connector->getID(), buffer_.get(), (std::uint32_t)length);
  }
}

} // namespace network
} // namespace kratos
