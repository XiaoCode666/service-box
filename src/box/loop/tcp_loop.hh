#pragma once

#include "util/box_std_allocator.hh"
#include "loop.hh"

namespace kratos {
namespace service {
class BoxNetwork;
}
} // namespace kratos

// 前置声明，依赖knet
typedef struct _loop_t kloop_t;
typedef struct _channel_ref_t kchannel_ref_t;

namespace kratos {
namespace loop {

using ListenerNameMap =
    kratos::service::PoolUnorederedMap<std::uint64_t,
                                       std::pair<std::uint64_t, std::string>>;
/**
 * @brief TCP网络循环实现
 */
class TcpLoop : public Loop {
  service::BoxNetwork *network_{nullptr}; ///< 网络框架指针
  kloop_t *loop_{nullptr};                ///< 网络循环

  using ChannelMap =
      service::PoolUnorederedMap<std::uint64_t, kchannel_ref_t *>;
  ChannelMap thread_channel_map_; ///< 网络线程管道查找表

public:
  TcpLoop(service::BoxNetwork *network);
  virtual ~TcpLoop();
  virtual auto start() -> bool override;
  virtual auto stop() -> bool override;
  virtual auto worker_update() -> void override;
  virtual auto do_worker_event(const service::NetEventData &event_data)
      -> void override;

public:
  /**
   * @brief 获取网络框架
   * @return 网络框架指针
   */
  auto get_network() -> service::BoxNetwork *;
  /**
   * 获取工作线程的管道表容器
   *
   * \return 管道表容器
   */
  auto get_worker_channel_map() noexcept -> ChannelMap &;
  /**
   * 添加工作线程管道
   *
   * \param channel_id 管道ID
   * \param channel_ref 管道引用
   */
  auto add_worker_channel(std::uint64_t channel_id, kchannel_ref_t *channel_ref)
      -> bool;
};

} // namespace loop
} // namespace kratos
