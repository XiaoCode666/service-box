#pragma once

#include "box/loop/loop.hh"
#include "box/loop/udp/udp_channel_loop.hh"
#include "kcp/ikcp.h"
#include "util/box_std_allocator.hh"
#include <list>
#include <memory>
#include <unordered_map>

namespace kratos {
namespace service {
class BoxNetwork;
}
} // namespace kratos

namespace kratos {
namespace loop {

/// KCP网络循环
class KcpLoop : public Loop {
  // KCP管道
  struct KcpChannel {
    // 构造
    // @param loop UDP网络管道循环
    // @param channel UDP管道ID
    KcpChannel(network::UdpChannelLoop *loop, std::uint64_t channel);
    // 析构
    ~KcpChannel();
    ikcpcb *kcp_{nullptr};                   // KCP实例
    network::UdpChannelLoop *loop_{nullptr}; // UDP网络管道循环
    std::uint64_t channel_{0};               // UDP管道ID
    // KCP输出回调
    static int output(const char *buf, int len, ikcpcb *kcp, void *user);
    // KCP会话ID, 底层已经进行了会话管理, 不同KCP使用同一个ID
    static const std::uint32_t KCPCONV = 0x10101010;
  };
  using KcpChannelPtr = std::shared_ptr<KcpChannel>;
  using UdpChannelLoopPtr = std::shared_ptr<network::UdpChannelLoop>;
  using ChannelMap = service::PoolUnorederedMap<std::uint64_t, KcpChannelPtr>;
  UdpChannelLoopPtr loop_;                ///< UDP网络循环
  ChannelMap channelMap_;                 ///< KCP管道管理
  service::BoxNetwork *network_{nullptr}; ///< 网络框架指针

public:
  KcpLoop(service::BoxNetwork *network);
  virtual ~KcpLoop();
  virtual auto start() -> bool override;
  virtual auto stop() -> bool override;
  virtual auto worker_update() -> void override;
  virtual auto do_worker_event(const service::NetEventData &event_data)
      -> void override;

public:
  /**
   * @brief 获取网络框架
   * @return 网络框架指针
   */
  auto get_network() -> service::BoxNetwork *;

protected:
  auto do_listen_request(const service::NetEventData &event_data) -> void;
  auto do_connect_request(const service::NetEventData &event_data) -> void;
  auto do_send_request(const service::NetEventData &event_data) -> void;
  auto do_close_request(const service::NetEventData &event_data) -> void;
  auto on_accept(std::uint64_t channel_id, const std::string &name) -> void;
  auto on_receive(std::uint64_t channel_id, const char *data,
                  std::uint32_t size) -> void;
  auto get_buffer_ptr(std::size_t length) -> char *;
};

} // namespace loop
} // namespace kratos
