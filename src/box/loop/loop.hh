#pragma once

#include <memory>
#include <string>

namespace kratos {
namespace service {
struct NetEventData;
}
} // namespace kratos

namespace kratos {
namespace loop {

/**
 * @brief 通讯层主循环接口
 */
class Loop {
public:
  virtual ~Loop() {}
  /**
   * @brief 在逻辑线程调用, 初始化
   * @return true成功,false失败
   */
  virtual auto start() -> bool = 0;
  /**
   * @brief 在逻辑线程调用, 停止并清理
   * @return true成功,false失败
   */
  virtual auto stop() -> bool = 0;
  /**
   * @brief 工作线程内调用, 运行主循环一次
   * @return
   */
  virtual auto worker_update() -> void = 0;
  /**
   * @brief 工作线程内调用, 处理逻辑线程发送过来的事件
   * @param event_data 事件
   * @return
   */
  virtual auto do_worker_event(const service::NetEventData &event_data)
      -> void = 0;
};

using LoopPtr = std::shared_ptr<Loop>;

/**
 * Loop工厂
 */
class LoopFactory {
public:
  virtual ~LoopFactory() {}
  /**
   * 获取Loop类型
   */
  virtual auto get_type() -> const std::string & = 0;
  /**
   * 建立Loop
   * @return Loop实例
   */
  virtual auto create_loop() -> LoopPtr = 0;
};

using LoopFactoryPtr = std::shared_ptr<LoopFactory>;

} // namespace loop
} // namespace kratos
