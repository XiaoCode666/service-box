#pragma once

#include <ostream>
#include <string>
#include <cstdio>

#if (defined(WIN32) || defined(_WIN64))
#include <windows.h>
#endif // (defined(WIN32) || defined(_WIN64))

namespace k {
namespace test {

static inline std::ostream& red(std::ostream& ostr) {
#if (defined(WIN32) || defined(_WIN64))
    auto handle = ::GetStdHandle(STD_OUTPUT_HANDLE);
    ::SetConsoleTextAttribute(handle, FOREGROUND_RED);
#else
	printf("\033[31m");
#endif // (defined(WIN32) || defined(_WIN64))
    return ostr;
}

static inline std::ostream& green(std::ostream& ostr) {
#if (defined(WIN32) || defined(_WIN64))
    auto handle = ::GetStdHandle(STD_OUTPUT_HANDLE);
    ::SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
#else
	printf("\033[32m");
#endif // (defined(WIN32) || defined(_WIN64))
    return ostr;
}

static inline std::ostream& black(std::ostream& ostr) {
#if (defined(WIN32) || defined(_WIN64))
    auto handle = ::GetStdHandle(STD_OUTPUT_HANDLE);
    ::SetConsoleTextAttribute(handle, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
#else
	printf("\033[0m");
#endif // (defined(WIN32) || defined(_WIN64))
    return ostr;
}

}
}
