#!/bin/bash

#MIT License
#
#Copyright (c) 2020 cloudguan rcloudguan@163.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

OSNAME="Unknown"
ISN="Unknown"
CURPATH=`pwd`

function getLinuxDistribution() {
    #采用最简单和愚蠢的做法，不搞花活
    #最简单的判断，是否有os-release文件
    if type lsb_release > /dev/null 2>&1; then
        #是否有lsb-release文件
        OSNAME=$(lsb_release -si)
        ISN=apt-get
    elif [ -f /etc/os-release ]; then
	    . /etc/os-release
	    OSNAME=$NAME
	    ISN=apt-get
    elif [ -f /etc/debian_version ]; then
        #是否是debian
        OSNAME=Deepin
        ISN=apt-get
    elif [ -f /etc/rehat_version ]; then
        #是否是redhat
        OSNAME=RedHat
        ISN=yum
    else
        echo "unkown linux distribution!"
    fi
}

#前置环境安装，编译需要旧环境
function initenv() {
    echo ============init protoc================
    sudo $ISN install libtool -y 
    sudo $ISN install autoconf -y 
}

function down_protobuf(){
    echo "protobuff 开始下载 版本3.10.0"
    wget https://github.com/protocolbuffers/protobuf/archive/v3.10.0.tar.gz
    if [ $? -gt 0 ]
    then
        echo "下载失败请手动下载protobuf 在执行脚本"
        exit -1
    fi
}

function install_proto(){
    echo "开始解压"
    if [ ! -d ./protobuf-3.10.0 ]
    then
        tar zxvf v3.10.0.tar.gz
        if [ $? -gt -0 ]
        then
            echo "解压失败 请检查文件是否正确"
            exit 1
        fi 
    fi

    cd protobuf-3.10.0

    ./autogen.sh

    if [ -d ./build ]; then
        rm -rf build
    fi
    mkdir build

    cd build
    echo "开始配置 ../configure --prefix=/usr/local/protobuf --enable-shared CXXFLAGS=-fPIC"
    ../configure --bindir=${CURPATH}/../thirdparty/bin --includedir=${CURPATH}/../thirdparty/include --libdir=${CURPATH}/../thirdparty/lib/linux --enable-shared CXXFLAGS="-fPIC"

    if [ $? -gt 0 ]
    then
        echo "--prefix=/usr/local/protobuf --enable-shared CXXFLAGS=-fPIC 执行失败，检查log文件"
        exit 1
    fi 

    make -j4
    make install 

    if [ $? -gt 0 ]
    then
        echo "install protobuff failed!!"
        exit 1
    fi 

    echo "replace system protoc with ourself's"
    if [ -L /usr/local/bin/protoc ];then 
        sudo rm /usr/local/bin/protoc
    fi 
    sudo ln -s ${CURPATH}/../thirdparty/bin/protoc /usr/local/bin/protoc

    #这里比较愚蠢所有google的东西include 都在一个目录下，所有这里需要特殊判断一下 不能莽直接删！！！！！
    #if [ ! -d /usr/local/include/google ];then
    #    mkdir /usr/local/include/google
    #fi

    #if [ -f /usr/local/include/google/protobuf ];then
    #    rm /usr/local/include/google/protobuf
    #fi 
    #ln -sv /usr/local/protobuf/include/google/protobuf /usr/local/include/google/protobuf 
    # 库文件
    #if [ -f /usr/local/lib/libprotobuf.a ];then
    #    rm /usr/local/lib/libprotobuf.a
    #fi 
    #ln -s /usr/local/protobuf/lib/libprotobuf.a /usr/local/lib/libprotobuf.a
}

#check protobuff version and replace with v3.10.0
if [ -x "$(command -v protoc)" ]; then
### get version code
echo "已经安装protoc 请自行确定版本是否正确 最小版本 3.10"
##echo `protoc --version`
fi

#check current dir
echo "cur dir : ${CURPATH}" 


#检查操作系统版本 如果是unkown 就直接报错
getLinuxDistribution

if [[ $OSNAME == "Debian GNU/Linux" ]];then
    OSNAME="Debain"
    echo "osname set to debain"
fi

if [ $OSNAME = "Unknown" ]; then
    echo "unkwon linux version!!"
    exit 1
fi


#init 
initenv

echo "开始下载proto 文件"
if [ ! -d /data ]
then
    echo "data 目录不存在"
    exit 1
fi

if [ ! -f /data/thirdparty ]
then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
fi

# download gcc from thirparty
cd /data/thirdparty 

if [ ! -f v3.10.0.tar.gz ]
then
    down_protobuf
fi

install_proto

