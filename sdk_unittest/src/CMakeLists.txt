cmake_minimum_required(VERSION 3.10)
set(CMAKE_CXX_STANDARD 17)
project (box_sdk_unittest)

add_subdirectory(../../src/box_network ../../src/box_network)
add_subdirectory(../../src/sdk ../../src/sdk)

SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin/)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/../ ${PROJECT_SOURCE_DIR}/../../src ${PROJECT_SOURCE_DIR}/../../src/sdk ${PROJECT_SOURCE_DIR}/../../src/repo/src/include/root ${PROJECT_SOURCE_DIR}/../../src/repo/src/include/example ${PROJECT_SOURCE_DIR}/../../thirdparty/ ${PROJECT_SOURCE_DIR}/../../thirdparty/include)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/../../unittest/framework)

IF (MSVC)
    foreach(var
        CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO
        CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO
        )
        if(${var} MATCHES "/MD")
            string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")
        endif()
    endforeach()
ELSE()
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")
        set(CMAKE_CXX_COMPILER "clang++")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")
        set(CMAKE_CXX_COMPILER "g++")
    endif()
ENDIF ()

set(SRC_LIST 
	${PROJECT_SOURCE_DIR}/../../unittest/framework/main.cpp
	${PROJECT_SOURCE_DIR}/../../unittest/framework/fixture.cpp
	${PROJECT_SOURCE_DIR}/../../unittest/framework/testcase.cpp
	${PROJECT_SOURCE_DIR}/../../unittest/framework/unittest.cpp
	${PROJECT_SOURCE_DIR}/../../src/util/string_util.cpp
	${PROJECT_SOURCE_DIR}/../../src/util/os_util.cpp
)
aux_source_directory(${PROJECT_SOURCE_DIR}/test SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/jsoncpp SRC_LIST)
add_executable(sdk_frame_unittest ${SRC_LIST})

IF (MSVC)
    set_target_properties(sdk_frame_unittest PROPERTIES COMPILE_FLAGS "/EHa /D DISABLE_TRACE /D WIN32_LEAN_AND_MEAN /D _WINSOCK_DEPRECATED_NO_WARNINGS")
ELSE ()
    set_target_properties(sdk_frame_unittest PROPERTIES COMPILE_FLAGS "-DDISABLE_TRACE")
ENDIF ()

target_compile_definitions(sdk_frame_unittest PUBLIC
$<$<CONFIG:Debug>: -DDEBUG -D_DEBUG>
$<$<CONFIG:Release>: -DRELEASE>
)
